extern crate libc;
extern crate seccomp_sys;
use std::os::raw::{c_char, c_void};
use libc::{uid_t, gid_t};

fn main() 
{
    let mut args = std::env::args();
    args.next(); //ignores program name

    let program_name = args.next().expect("expect program");
    let dir          = args.next().expect("expect directory");
    let uid          = args.next().expect("expect uid");
    let uid = uid.parse::<u32>().expect("invalid uid");
    let gid          = args.next().expect("expect gid");
    let gid = gid.parse::<u32>().expect("invalid gid");

    let argv: Vec<_> = args.map(|s| s.as_ptr() as *const c_char).collect();

    unsafe
    {
        assert!(libc::chroot(dir.as_ptr() as * const c_char) == 0);
        assert!(libc::setgid(gid as gid_t)== 0);
        assert!(libc::setuid(uid as uid_t)== 0);

        //assert!(libc::prctl(libc::PR_SET_SECCOMP, libc::SECCOMP_MODE_STRICT, 0, 0 as *const c_void) == 0);

        let ctx = seccomp_sys::seccomp_init(seccomp_sys::SCMP_ACT_ERRNO(5));
        seccomp_sys::seccomp_rule_add(ctx, seccomp_sys::SCMP_ACT_ALLOW, 59, 0); 
        seccomp_sys::seccomp_rule_add(ctx, seccomp_sys::SCMP_ACT_ALLOW, 0, 0); 
        seccomp_sys::seccomp_rule_add(ctx, seccomp_sys::SCMP_ACT_ALLOW, 1, 0); 
        seccomp_sys::seccomp_rule_add(ctx, seccomp_sys::SCMP_ACT_ALLOW, 20, 0); 
        seccomp_sys::seccomp_rule_add(ctx, seccomp_sys::SCMP_ACT_ALLOW, 60, 0); 
        seccomp_sys::seccomp_rule_add(ctx, seccomp_sys::SCMP_ACT_ALLOW, 20, 0); 
        seccomp_sys::seccomp_rule_add(ctx, seccomp_sys::SCMP_ACT_ALLOW, 257, 0); 
        seccomp_sys::seccomp_rule_add(ctx, seccomp_sys::SCMP_ACT_ALLOW, 17, 0); 
        seccomp_sys::seccomp_rule_add(ctx, seccomp_sys::SCMP_ACT_ALLOW, 3, 0); 
        seccomp_sys::seccomp_rule_add(ctx, seccomp_sys::SCMP_ACT_ALLOW, 5, 0); 
        seccomp_sys::seccomp_rule_add(ctx, seccomp_sys::SCMP_ACT_ALLOW, 9, 0); 
        seccomp_sys::seccomp_rule_add(ctx, seccomp_sys::SCMP_ACT_ALLOW, 158, 0); 
        seccomp_sys::seccomp_rule_add(ctx, seccomp_sys::SCMP_ACT_ALLOW, 10, 0); 
        seccomp_sys::seccomp_load(ctx);

        libc::execv(program_name.as_ptr() as *const c_char, argv.as_ptr());
    }
}
